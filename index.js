const fs = require('fs');
const path = require('path');
const util = require('util');
const readline = require('readline-sync');
const regulars = require('./regulars')
const colors = require("./colors")
const CodeAnalyzer = require("./sast").CodeAnalyzer
const OSINTAnalyzer = require("./osint").OSINTAnalyzer

class App
{
    async run()
    {
                                                                  
      const multiLineText = `             _           _        _     ___           _         
 ___ ___ ___| |_ ___ ___| |_    _| |___|  _|___ ___ _| |___ ___ 
| . |  _| . |  _| -_|_ -|  _|  | . | -_|  _| -_|   | . | -_|  _|
|  _|_| |___|_| |___|___|_|    |___|___|_| |___|_|_|___|___|_|   v0.1.1
|_|                                                             
    `;
    
    console.log(multiLineText);

  while (true){  
      const name = readline.question("Select an action:\n(1) Scan by SAST\n(2) Scan by OSINT\n(3) Scan for regular phrases\n(4) Try to fix\n>>> ");
      if (name == "1")
      {
        const folderPath = readline.question("Select which folder you want to scan: ");
        const analyzer = new CodeAnalyzer(folderPath);
        await analyzer.analyzeCodeInFolder();
      }
      else if (name == "2")
      {
        const folderPath = readline.question("Select which folder you want to scan: ");
        const analyzer = new OSINTAnalyzer(folderPath);
        await analyzer.startAnalysis();
      }
      else if (name == "3")
      {
        const folderPath = readline.question("Select which folder you want to scan: ");
        const analyzer = new CodeAnalyzer(folderPath);
        await analyzer.analyzeRegularsInFolder();
      }
      else if (name == "4")
      {
        const folderPath = readline.question("Select which folder you want to fix: ");
        const analyzer = new CodeAnalyzer(folderPath);
        await analyzer.copyFolder(folderPath, folderPath + "_fixed");
        await analyzer.fixCodeInFolder();
      }

  }
    }
}

const app = new App();
app.run()