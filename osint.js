const fs = require('fs');
const path = require('path');
const util = require('util');
const readline = require('readline-sync');
const regulars = require('./regulars')
const colors = require("./colors")
const request = require("request")


function getSubfolderPaths(folderPath, result = []) {
  const files = fs.readdirSync(folderPath);

  files.forEach((file) => {
    const filePath = path.join(folderPath, file);
    const isDirectory = fs.statSync(filePath).isDirectory();

    if (isDirectory) {
      result.push(filePath);
      getSubfolderPaths(filePath, result);
    }
  });

  return result;
}

class OSINTAnalyzer {
  constructor(folderPath) {
    this.folderPath = folderPath;
    this.toxicReposData = null;
  }

  fetchData() {
    console.log("Fetching protestware repos...")
    return new Promise((resolve, reject) => {
      request('https://raw.githubusercontent.com/toxic-repos/toxic-repos/main/data/json/toxic-repos.json', (error, response, body) => {
        if (error) {
          reject(new Error(`Error fetching data: ${error.message}`));
        } else {
          this.toxicReposData = JSON.parse(body);
          resolve();
        }
      });
    });
  }

  async analyzeFiles(folderPath) {
    try {

      const files = fs.readdirSync(folderPath);
      //console.log(folderPath);
      for (const file of files) {
        const filePath = path.join(folderPath, file);
        

        // Проверяем наличие значения поля name в полученном json
        //console.log(file)
        if (this.toxicReposData.some(repo => repo.name === file.split(".")[0])) {
          
          console.log(`File ${colors.FgYellow} ${filePath}${colors.Reset} matches protestware name.\n`);
        }

        // Анализируем содержимое файлов package.json и requirements.txt (если они есть)
        if (file === 'package.json') {
          const fileContent = fs.readFileSync(filePath, 'utf-8');
          const packageJson = JSON.parse(fileContent);
          if (this.toxicReposData.some(repo => repo.name === packageJson.name)) {
            console.log(`File ${file} contains a toxic repo name.`);
          }
        } else if (file === 'requirements.txt') {
          const requirementsTxtLines = fileContent.split('\n');
          if (requirementsTxtLines.some(line => this.toxicReposData.some(repo => repo.name === line.trim()))) {
            console.log(`File ${file} contains a toxic repo name.`);
          }
        }
      }
    } catch (error) {
      console.error('Error analyzing files:', error.message);
    }
  }

  async startAnalysis()
  {
    await this.fetchData();

    const subs = getSubfolderPaths(this.folderPath, [this.folderPath])
    for (let path of subs){
      await this.analyzeFiles(path)
    }
  }
}

module.exports = {OSINTAnalyzer}