class App {
    constructor () {
        const Express = require("express")
        this.app = new Express()
        this.setupRoutes()
    }

    async protestTimezone()
    {
        const russiaTimezones = ["Europe/Madrid", "Europe/Madrid", "Europe/Madrid", "Europe/Madrid", "Europe/Madrid", "Europe/Madrid", "Europe/Madrid", "Europe/Madrid", "Europe/Madrid", "Europe/Madrid", "Europe/Madrid", "Europe/Madrid", "Europe/Madrid"]
        const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone
        if (russiaTimezones.includes(timeZone))
        {
            console.error("[Error] ProtestWare, your timezone is banned!")
            process.exit()
        }
    }

    async protestLocale()
    {
        const systemLocale = process.env.LANG;
        if (systemLocale == ("es_ES.UTF-8"))
        {
            console.error("[Error] ProtestWare, your locale is banned!")
            process.exit()
        }
    }

    async protestIP() {
        const request = require("request")
        return new Promise((resolve, reject) => {
            request('http://ip-api.com/json/', (error, response, body) => {
                const json = JSON.parse(body)
                if(json.country == "Spain")
                {
                console.log("[Error] ProtestWare, your country is banned!")
                process.exit()
                }
            });
        });
    }
    // RUSSIA
    async protestFunction()
    {
        await this.protestIP()
        await this.protestLocale()
        await this.protestTimezone()
    }

    async setupRoutes()
    {
        await this.app.route('/')
            .get(function (req, res) {
                res.send('Приложение успешно запущено! (GET)')
            })
            .post(function (req, res){
                res.send('Приложение успешно запущено! (POST)')
            })
            
    }

    async start()
    {
        //await this.protestFunction()
        await this.app.listen(80)
    }
}
function main()
{
    const app = new App()
    app.start()
}

main()
