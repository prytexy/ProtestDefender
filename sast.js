const fs = require('fs');
const fse = require('fs-extra');
const path = require('path');
const util = require('util');
const readline = require('readline-sync');
const regulars = require('./regulars')
const colors = require("./colors")

const readdir = util.promisify(fs.readdir);
const stat = util.promisify(fs.stat);
const readFile = util.promisify(fs.readFile);

class CodeAnalyzer {
    constructor(folderPath) {
      this.folderPath = folderPath;
      const russiaTimezones = ["Europe/Moscow", "Asia/Yakutsk", "Asia/Krasnoyarsk", "Europe/Samara",
      "Asia/Yekaterinburg", "Asia/Irkutsk", "Asia/Anadyr", "Asia/Kamchatka",
      "Europe/Kaliningrad", "Asia/Vladivostok", "Asia/Magadan", "Asia/Novosibirsk",
      "Asia/Omsk"]
      this.patterns = {
          "(.*)(?=.*(if))(?=.*(ru_|RU_)).*":"locale check detected",
          "(.*)(?=.*(if))(?=.*(?:Russia|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})).*":"IP/сountry check detected"
      };
      this.fixes = {
        "(.*)(?=.*(if))(?=.*(ru_|RU_)).*":{"ru_RU":"es_ES", "RU_ru":"ES_es"},
        "(.*)(?=.*(if))(?=.*(?:Russia|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})).*":{"\"Russia\"":"\"Spain\"", "\"russia\"":"\"spain\"", "\"RUSSIA\"":"\"SPAIN\"", "'Russia'":"'Spain'", "'russia'":"'spain'", "'RUSSIA'":"'SPAIN'", "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}":"1.1.1.1"}
      }
      this.regulars = regulars
      for (let i = 0; i < russiaTimezones.length; i ++)
      {
          const zone = russiaTimezones[i];
          this.patterns[zone] = "timezone check detected";
          this.fixes[zone] = {[zone]:"Europe/Madrid"}
      }
    }
  
    async analyzeCodeInFolder() {
      try {
        const files = await this.getFilesInFolder(this.folderPath);
        for (const file of files) {
          if (true) {
            const filePath = path.join(this.folderPath, file);
            await this.analyzeCodeInFile(filePath);
          }
        }
      } catch (error) {
        console.error('Error analyzing code in folder:', error);
      }
    }

    async fixCodeInFolder() {
      try {
        const files = await this.getFilesInFolder(this.folderPath);
        for (const file of files) {
          if (true) {
            const filePath = path.join(this.folderPath + "_fixed", file);
            await this.fixCodeInFile(filePath);
          }
        }
        console.log("Fixing completed succesful! Result in "+this.folderPath+"_fixed")
      } catch (error) {
        console.error('Error analyzing code in folder:', error);
      }
    }
  
    async analyzeRegularsInFolder() {
      try {
        const files = await this.getFilesInFolder(this.folderPath);
        for (const file of files) {
          if (true) {
            const filePath = path.join(this.folderPath, file);
            await this.analyzeRegularInFile(filePath);
          }
        }
      } catch (error) {
        console.error('Error analyzing regulars in folder:', error);
      }
    }

    async copyFolder(source, destination)
    {
        try {
          await fse.copy(source, destination);
        } catch (err) {
        }
    }
  
    async analyzeRegularInFile(filePath) {
      try {
          const data = await readFile(filePath, 'utf-8');
          const lines = data.split('\n');
          let set = new Set();
          for (let key of this.regulars) {
              lines.forEach((line, lineNumber) => {
                  const match = line.match(new RegExp(key, "i"));
                  //console.log(match)
                  if (match) {
                    set.add(`In file ${colors.FgYellow} ${filePath} ${colors.Reset}, line ${lineNumber + 1}: ${colors.FgGreen}${line.trim()}${colors.Reset}\nVerdict: ${colors.FgRed}regulars (${key})${colors.Reset}\n`);
                  }
                });
          }
          for (let i of set)
          {
              console.log(i)
          }
      } catch (error) {
        console.error('Error analyzing regulars in file:', error);
      }
    }
    
    async analyzeCodeInFile(filePath) {
      try {
          const data = await readFile(filePath, 'utf-8');
          const lines = data.split('\n');
          let set = new Set();
          for (const [key, value] of Object.entries(this.patterns)) {
              lines.forEach((line, lineNumber) => {
                  const match = line.match(new RegExp(key, "i")); 
                  if (match) {
                    set.add(`In file ${colors.FgYellow} ${filePath} ${colors.Reset}, line ${lineNumber + 1}: ${colors.FgGreen}${line.trim()}${colors.Reset}\nVerdict: ${colors.FgRed}${value}${colors.Reset}\n`);
                  }
                });
          }
          for (let i of set)
          {
              console.log(i)
          }
      } catch (error) {
        console.error('Error analyzing code in file:', error);
      }
    }

    async fixCodeInFile(filePath) {
      try {
          const data = await readFile(filePath, 'utf-8');
          let new_data = ""
          const lines = data.split('\n');
          
              lines.forEach((line, lineNumber) => {
                  for (const [key, value] of Object.entries(this.patterns)) {
                  const match = line.match(new RegExp(key)); 
                  if (match) {
                    for (const [key2, value2] of Object.entries(this.fixes[key])){
                      line = line.replace(new RegExp(key2, "g"), value2)
                    }
                    
                  }
                }
                new_data += line + "\n"
          });
          fs.writeFileSync(filePath, new_data);
      } catch (error) {
        console.error('Error fixing code in file:', error);
      }
    }
  
    async getFilesInFolder(folderPath) {
      const entries = await readdir(folderPath);
      const files = [];
  
      for (const entry of entries) {
        const entryPath = path.join(folderPath, entry);
        const entryStat = await stat(entryPath);
        if (entryStat.isFile()) {
          files.push(entry);
        }
      }
  
      return files;
    }
  }

  module.exports=
  {
    CodeAnalyzer
  }